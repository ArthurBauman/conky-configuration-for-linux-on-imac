# Conky Configuration iMac

![conky1.png](./images/conky1.png)
# Conky configuration #

This is a configuration file for Conky running on a dual boot (Mac OS, Linux) iMAC.
Conky is a system monitor (and more). Conky is free software and runs in X on Linux and BSD. Website: [http://conky.sourceforge.net/aboutconky.html](http://conky.sourceforge.net/aboutconky.html)

# Install #

First install and start Conky. See the [Conky documentation](http://conky.sourceforge.net/documentation.html).
After that put the .conkyrc somewhere Conky can find it. Mostly in ~/.conkyrc. Put the .conky.pl in the same directory.
This configuration needs the 'sensors' (> 3.6.0) command and the Perl interpreter installed. 

# Author #

Arthur Bauman  
[LinkedIn](https://www.linkedin.com/in/arthurbauman)  
[Website](http://bauman.nu)