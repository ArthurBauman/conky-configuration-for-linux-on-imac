#!/usr/bin/perl -T
use strict;
use warnings;
use JSON qw( decode_json );
use utf8;
use Scalar::Util qw(looks_like_number);

binmode STDOUT, ":utf8";

# This programm creates Lua code for Conky by reading sensors command.
# Give system 'sensors' command and adjust this script according your situation.
# Author: Arthur Bauman
# Website: https://www.bauman.nu

##################
##### CONFIG #####
##################

# Path to 'sensors command'.
$ENV{PATH} = "/usr/bin/";

# Fans (See system 'sensors' command).
my @fans = ('ODD', 'HDD', 'CPU');

# Number of CPU cores.
my $coreNumber = 2;

# Maximum core temp also can be read from sensors command. I prefer a lower temperature.
my $maxCoreTemp = 90;

my $maxSensorsTemp = 90;

# Sensor names (iMac): https://superuser.com/questions/553197/interpreting-sensor-names
#              https://web.archive.org/web/20150107044816/http://www.parhelia.ch/blog/statics/k3_keys.html
#              https://web.archive.org/web/20160525201657/http://jbot-42.github.io/Articles/smc.html

my @sensors = (
    ['TA0P', 'Airflow'],
    ['TC0D', 'CPU Diode Analog'],
    ['TC0H', 'CPU Heatsink'],
    ['TC0P', 'CPU Proximity'],
    ['TG0D', 'GPU Die'],
    ['TG0H', 'GPU Heatsink'],
    ['TG0P', 'GPU Proximity'],
    ['TH0P', 'HDD Bay'],
    ['TL0P', 'LCD Proximity'],
    ['TN0D', 'Northbridge Die'],
    ['TN0H', 'Mem. Contr. Hub Heatsink'],
    ['TN0P', 'Northbridge Proximity'],
    ['TO0P', 'ODD Proximity'],
    ['Tm0P', 'Memory Bank'],
    ['Tp0P', 'Platform Contr. Hub Prox.'],
);

######################
##### END CONFIG ##### 
######################

my $fanRpmTxt;
my $coreTempTxt;
my $sensorsTempText;

my $data;
# Scope 'slurp' mode.
{
    # Enable 'slurp' mode.
    local $/ = undef; 
    open(my $fh, '-|', 'sensors -j') or die $!;
    my $json = <$fh>;
    close $fh;
    $data = decode_json($json);
}

# CPU Core temperature.
# ToDo: Add code for temperature below zero.
for (my $i = 0; $i < $coreNumber; $i++) {
    my $core = 'Core ' . $i;
    my $coreTemp = $data->{'coretemp-isa-0000'}->{$core}->{'temp' . ($i + 2) . '_input'};
    if ($coreTemp > $maxCoreTemp) {
        $coreTempTxt .= '${color0}' . "$core". ':${alignr}' . $coreTemp . '°C${color}' . "\n";
    }
    else {
        $coreTempTxt .= "$core" . ':${alignr}' . $coreTemp . '°C' . "\n";
    }
}

# Fans
if (scalar(@fans)) {
    my $i = 1;
    for my $fan (@fans) {
        my $fanRpmMin = $data->{'applesmc-isa-0300'}->{$fan . ' '}->{'fan' . $i . '_min'};
        my $fanRpmMax = $data->{'applesmc-isa-0300'}->{$fan . ' '}->{'fan' . $i . '_max'};
        my $fanRpm    = $data->{'applesmc-isa-0300'}->{$fan . ' '}->{'fan' . $i . '_input'};
        # Keep some inaccuracy in mind ("- 10" and "+ 10").
        if ($fanRpm < $fanRpmMin - 10 || $fanRpm > $fanRpmMax + 10) {
            $fanRpmTxt .= '${color0}' . $fan . ':${alignr}' . $fanRpm . ' RPM${color}' . "\n";
        }
        else {
            $fanRpmTxt .= "$fan". ':${alignr}' . $fanRpm . ' RPM' . "\n";
        }
        $i++;
    }
}

# Other temperature sensors.
# ToDo: Add code for temperature below zero.
if (scalar(@sensors)) {
    my $i = 1;
    for my $sensor (@sensors) {
        my $temp = $data->{'applesmc-isa-0300'}->{@$sensor[0]}->{'temp' . $i . '_input'};
        #Round
        $temp = sprintf "%.0f", $temp;
        if (!looks_like_number($temp)) {
           $sensorsTempText .=  '${color0}' . "@$sensor[1]" . ':${alignr}Can\'t read temperature.${color}' . "\n"; 
        }
        elsif ($temp > $maxSensorsTemp) {
            $sensorsTempText .=  '${color0}' . "@$sensor[1]" . ':${alignr}' . "$temp" . '°C${color}' . "\n";
        }
        else {
            $sensorsTempText .=  "@$sensor[1]" . ':${alignr}' . "$temp" . '°C' . "\n";
        }
        $i++;
    }
}

# Create Lua code for Conky
print '${alignc}${font :bold}FAN${font}' . "\n";
print "$fanRpmTxt" . "\n";
print '${alignc}${font :bold}TEMP${font}'  . "\n";
print "$coreTempTxt";
print "$sensorsTempText";